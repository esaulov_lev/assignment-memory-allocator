#define _DEFAULT_SOURCE
#include <unistd.h>
#include "mem.h"
#include "util.h"
#include "mem_internals.h"

#define HEAP_SIZE 10000

static void *test_heap_init(){
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        printf( "Failed to initialize the heap");
    }
    return heap;
}

void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

void test1(struct block_header *base_block){
    printf( "Test 1 started\n");
    void *memory = _malloc(HEAP_SIZE/10);
    if (memory == NULL)
        printf("Test 1 failed: malloc returns NULL\n");
    debug_heap(stdout, base_block);
    if (base_block->capacity.bytes != HEAP_SIZE/10 || base_block->is_free != false)
        printf("Test 1 failed \n");
    _free(memory);
    printf( "Test 1 passed\n");
}

void test2(struct block_header *base_block){
    printf("Test 2 started\n");
    void *memory1 = _malloc(HEAP_SIZE/10), *memory2 = _malloc(HEAP_SIZE/10);
    if (!memory1 || !memory2)
        printf("Test 2 failed: malloc returns NULL\n");
    _free(memory1);
    debug_heap(stdout, base_block);
    struct block_header *header1 = block_get_header(memory1);
    struct block_header *header2 = block_get_header(memory2);
    if (!header1->is_free || header2->is_free)
        printf("Test 2 failed\n");
    _free(memory1);
    _free(memory2);
    printf("Test 2 passed\n");
}

void test3(struct block_header *base_block){
    printf("Test 3 started\n");
    void *memory1 = _malloc(HEAP_SIZE/10);
    void *memory2 = _malloc(HEAP_SIZE/10);
    void *memory3 = _malloc(HEAP_SIZE/10);
    if (!memory1 || !memory2 || !memory3)
        printf("Test 3 failed: malloc returns NULL\n");
    _free(memory2);
    _free(memory1);
    debug_heap(stdout, base_block);
    struct block_header *header1 = block_get_header(memory1);
    struct block_header *header2 = block_get_header(memory2);
    struct block_header *header3 = block_get_header(memory3);

    if (!header1->is_free || !header2->is_free || header3->is_free)
        printf("Test 3 failed");
    if (header1->capacity.bytes < (HEAP_SIZE/5) + offsetof(struct block_header, contents))
    printf("Test 3 failed\n");
    printf("Test 3 passed\n");
}

void test4(struct block_header *base_block) {
    printf("Test 4 started\n");
    void *memory1 = _malloc(HEAP_SIZE);
    struct block_header *header1 = block_get_header(memory1);
    if (!header1){
        printf("Test 4 failed\n");
        return;
    }
    if (header1->capacity.bytes < HEAP_SIZE){
        printf("Test 4 failed\n");
        return;
    }
    _free(header1->contents);
    debug_heap(stdout, base_block);
    printf("Test 4 passed\n");
}

void test5(struct block_header *base_block){
    printf("Test 5 started\n");
    void *memory1 = _malloc(HEAP_SIZE*10);
    if (!memory1){
        printf("Test 5 failed\n");
        return;
    }
    struct block_header* header = base_block;
    while(header->next != NULL)
        header = header->next;
    debug_heap(stdout, base_block);
    void const* new_addr = (uint8_t *) header + size_from_capacity(header->capacity).bytes;
    map_pages(new_addr, 1000, MAP_FIXED);
    debug_heap(stdout, base_block);
    void* memory2 = _malloc(4*HEAP_SIZE);
    debug_heap(stdout, base_block);
    struct block_header* block = block_get_header(memory2);
    if (block == header)
        printf("Test 5 failed\n");
    _free(memory1);
    _free(memory2);
    printf("Test 5 passed\n");

}

bool run_all_tests(){
    struct block_header *base_block = (struct block_header *) test_heap_init();
    test1(base_block);
    test2(base_block);
    test3(base_block);
    test4(base_block);
    test5(base_block);
    return 1;
}

