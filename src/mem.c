#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    struct region all_reg;
    size_t reg = region_actual_size(query);
    void *new_addr =  map_pages(addr, reg, MAP_FIXED_NOREPLACE);
    if (new_addr == MAP_FAILED){
        new_addr = map_pages(addr,reg,0);
        all_reg = (struct region){new_addr, reg, 0};
    }
    else
        all_reg = (struct region){new_addr,reg, 1};
    block_init(new_addr, (block_size){reg}, NULL);
    return all_reg;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
    const struct region region = alloc_region( HEAP_START, initial );
    if ( region_is_invalid(&region) ) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
    return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (block_splittable(block,query) && block->is_free) {
        block_size block_sz =  {block->capacity.bytes - query};
        block->capacity.bytes = query;
        void* new_addr = block_after(block);
        block_init(new_addr, block_sz, block->next);
        block->next = new_addr;
        return 1;
    }
    return 0;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
    return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    struct block_header* header = block -> next;
    if (header && mergeable(block, header)){
        block->next = header->next;
        block->capacity.bytes += size_from_capacity(header->capacity).bytes;
        return  1;
    }
    return 0;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};


static struct block_search_result find_good_or_last ( struct block_header* restrict block, size_t sz ) {
    struct block_header * header1 = block;
    struct block_header * header2 = block;
    while (header1 != NULL) {
        if (header1->is_free && block_is_big_enough(sz, block)) {
            return (struct block_search_result) {
                    .type = BSR_FOUND_GOOD_BLOCK,
                    .block = header1
            };
        }
        header2 = header1;
        header1 = header1->next;
    }
    return (struct block_search_result) {
            .type = BSR_REACHED_END_NOT_FOUND,
            .block = header2
    };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result block_res = find_good_or_last(block, query);
    if (block_res.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(block_res.block, query);
        (block_res.block)->is_free = false;
    }
    return block_res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    void* new_addr = block_after(last);
    struct region new_reg = alloc_region(new_addr,query);
    if(region_is_invalid(&new_reg))
        return NULL;
    last->next = new_reg.addr;
    return new_reg.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result result = try_memalloc_existing(query, heap_start);
    if (result.type == BSR_FOUND_GOOD_BLOCK)
        return result.block;
    else if (result.type == BSR_REACHED_END_NOT_FOUND) {
        result.block = grow_heap(result.block, query);
        split_if_too_big(result.block, query);
    }
    else if (result.type == BSR_CORRUPTED)
        return NULL;
    result.block->is_free = false;
    return result.block;
}

void* _malloc( size_t query ) {
    struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
    if (addr) return addr->contents;
    else return NULL;
}

struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (!mem) return ;
    struct block_header* header = block_get_header(mem);
    header->is_free = 1;
    while(try_merge_with_next(header));
}
